#include "options.h"

int main(int argc, char *argv[]) {
	
	char buf[BUF_SIZ];

	//структуры для хранения заголовков
	struct ether_header *eh = (struct ether_header *) buf;
	struct iphdr *iph = (struct iphdr *) (buf + sizeof(struct ether_header));
	struct udphdr *udph = (struct udphdr *) (buf + sizeof(struct iphdr) + sizeof(struct ether_header));
	int sockfd;

	//открытие PF_PACKET сокета, слушающего только ETHERTYPE_IP
	if ((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETHERTYPE_IP))) == -1) {
		perror("listener: socket"); 
		return -1;
	}
	struct ifreq ifopts;
	strncpy(ifopts.ifr_name, DEFAULT_IF, IFNAMSIZ-1);
	ioctl(sockfd, SIOCGIFFLAGS, &ifopts);
	ifopts.ifr_flags |= IFF_PROMISC;
	ioctl(sockfd, SIOCSIFFLAGS, &ifopts);
	int sockopt;
	if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt)) == -1) {
		perror("setsockopt");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
	if (setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, DEFAULT_IF, IFNAMSIZ-1) == -1)  {
		perror("SO_BINDTODEVICE");
		close(sockfd);
		exit(EXIT_FAILURE);
	}
	unsigned int n = COUNT_CLIENTS;
	char *filtredIP[n];
	char bufip[BUF_SIZ] = CLIENT_IP;
	for(int i = 0; i < n; i++) {
		filtredIP[i] = malloc(sizeof(char)*strlen(bufip));
		strcpy(filtredIP[i], bufip);
	}

	struct ifreq if_ip; 
	struct sockaddr_storage their_addr;
	ssize_t numbytes;
	char sender[BUF_SIZ];
	uint8_t macserver[] = SERVER_MAC;

	while(1) {
		printf("Сервер: ожидание приёма...\n");
		numbytes = recvfrom(sockfd, buf, BUF_SIZ, 0, NULL, NULL);
		printf("Сервер: получен пакет %zd байт.\n", numbytes);

		//Проверка: предназначен ли пакет для сервера
		int check;
		check = checkingMAC(buf, sockfd);

		if(check == 1) {
			struct iphdr *iph = (struct iphdr *)(buf + sizeof(struct ether_header));
			((struct sockaddr_in *)&their_addr)->sin_addr.s_addr = iph->saddr;
			inet_ntop(AF_INET, &(((struct sockaddr_in*)&their_addr)->sin_addr), sender, sizeof(sender));
			if (n > 0) {
				for(int i = 0; i < n; i++) {
					if (strcmp(sender, filtredIP[i]) == 0) {
						infopacket(buf , numbytes);

						// Формирование ответа
						int socktocl;

						if ((socktocl = socket(AF_PACKET, SOCK_RAW, IPPROTO_UDP)) == -1) {
							perror("socket");
						}

						int text_len = 0;
						char sendbuf[BUF_SIZ];
						struct ether_header *ehcl = (struct ether_header*)sendbuf;

						memset(sendbuf, 0, BUF_SIZ);

						for(int i = 0; i < LENMAC; i++) {
							ehcl->ether_shost[i] = (uint8_t)eh->ether_dhost[i];
							ehcl->ether_dhost[i] = (uint8_t)eh->ether_shost[i];
						}
						ehcl->ether_type = (uint8_t)eh->ether_type;
						text_len += sizeof(struct ether_header);

						struct iphdr *iphcl = (struct iphdr *) (sendbuf + sizeof(struct ether_header));

						iphcl->ihl = iph->ihl;
						iphcl->version = iph->version;
						iphcl->tos = iph->tos;
						iphcl->id = iph->id;
						iphcl->ttl = iph->ttl;
						iphcl->protocol = iph->protocol;
						inet_ntop(AF_INET, &(iph->daddr), sender, sizeof(sender));
						iphcl->saddr = inet_addr(sender);
						inet_ntop(AF_INET, &(iph->saddr), sender, sizeof(sender));
						iphcl->daddr = inet_addr(sender);
						text_len += sizeof(struct iphdr);

						struct udphdr *udphcl = (struct udphdr *) (sendbuf + sizeof(struct iphdr) + sizeof(struct ether_header));

						udphcl->source = udph->dest;
						udphcl->dest = udph->source;
						udphcl->check = udph->check;
						text_len += sizeof(struct udphdr);

						for(int i = sizeof(struct ether_header)+sizeof(struct iphdr)+sizeof(struct udphdr); i < numbytes; i++) {
							sendbuf[text_len++] = buf[i]; 
						}

						udphcl->len = htons(text_len - sizeof(struct ether_header) - sizeof(struct iphdr));
						iphcl->tot_len = htons(text_len - sizeof(struct ether_header));

						struct sockaddr_ll socket_address;

						struct ifreq if_idx;
						
						memset(&if_idx, 0, sizeof(struct ifreq));
						strncpy(if_idx.ifr_name, DEFAULT_IF, IFNAMSIZ-1);

						
						if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0) {		 
							perror("SIOCGIFINDEX");
						}

						socket_address.sll_ifindex = if_idx.ifr_ifindex;

						socket_address.sll_halen = ETH_ALEN;

						for(int i = 0; i < LENMAC; i++) {
							socket_address.sll_addr[i] = (uint8_t)eh->ether_shost[i];
						}

						if (sendto(socktocl, sendbuf, text_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) != -1) {
							printf("Sended\n");
						}
						else {
							printf("Error: %s\n", strerror(errno));
						}
						
						close(socktocl);
					}
				}
			}
			else {
				/* Принятие всех пакетов пришедших на MAC-адрес */
				infopacket(buf, sockfd);
			}
		}
		else {
			continue;
		}
	}
	for(int i = 0; i < n; i++) {
		free(filtredIP[i]);
		strcpy(filtredIP[i], bufip);
	}
	close(sockfd);
	
	return 0;
}