run: main_server.c main_client.c options.c options.h
	gcc -std=gnu99 main_server.c options.c -o main_server -I.
	gcc -std=gnu99 main_client.c options.c -o main_client -I.
clean:
	rm *.o main_server main_client