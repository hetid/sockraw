#include "options.h"

int main(int argc, char const *argv[]) {
	//Открытие RAW-socket
	int sockfd;
	if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_UDP)) == -1) {
		perror("socket");
	}
	//Получение MAC-адреса 
	struct ifreq if_mac;   //MAC
	struct ifreq if_idx;   //Индекс интерфейса
	struct ifreq if_ip;	//IP-адрес
	//Заполнение поля ИМЕНИ интерфейса заданным
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, DEFAULT_IF, IFNAMSIZ-1);

	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, DEFAULT_IF, IFNAMSIZ-1);

	memset(&if_ip, 0, sizeof(struct ifreq));
	strncpy(if_ip.ifr_name, DEFAULT_IF, IFNAMSIZ-1);
	//Получение source MAC-адреса
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0) {
		perror("SIOCGIFHWADDR");
	}
	//Получение индекса выходного интерфейса
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0) {
		perror("SIOCGIFINDEX");
	}
	//Получение source IP-адреса
	if (ioctl(sockfd, SIOCGIFADDR, &if_ip) < 0) {
		perror("SIOCGIFADDR");
	}
	//Объявление вспомогательных переменных
	int text_len = 0;
	char sendbuf[BUF_SIZ];
	uint8_t macserver[] = SERVER_MAC;
	char destIP[] = SERVER_IP;
	
	//Создание и заполнение структуры для ETHERNET-заголовка
	struct ether_header *eh = (struct ether_header*)&sendbuf[0];
	for(int i = 0; i < LENMAC; i++) {
		eh->ether_shost[i] = ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[i];
		eh->ether_dhost[i] = macserver[i];
	}
	eh->ether_type = htons(ETHERTYPE_IP);
	text_len += sizeof(struct ether_header);

	//Создание и заполнение структуры для IP-заголовка
	struct iphdr *iph = (struct iphdr *) (sendbuf + sizeof(struct ether_header));
	iph->ihl = IHL;
	iph->version = VERSION_IP;
	iph->tos = TOS;
	iph->id = htons(ID_IP);
	iph->ttl = TTL;
	iph->protocol = IP_PROTO_FOR_UDP;
	iph->saddr = inet_addr(inet_ntoa(((struct sockaddr_in *)&if_ip.ifr_addr)->sin_addr));
	iph->daddr = inet_addr(destIP);
	text_len += sizeof(struct iphdr);

	//Создание и заполнение структуры для UDP-заголовка
	struct udphdr *udph = (struct udphdr *) (sendbuf + sizeof(struct iphdr) + sizeof(struct ether_header));
	udph->source = htons(UDP_CLIENT_PORT);
	udph->dest = htons(UDP_SERVER_PORT);
	udph->check = 0;
	text_len += sizeof(struct udphdr);

	//Заполнение поля данных
	char txt_bufer[BUF_SIZ];
	memset(txt_bufer, 0, sizeof(char)*BUF_SIZ);
	if (argc < 2){
		printf("Недостаточно аргументов! Введите сообщение!\n");
		scanf("%s", txt_bufer);
	} else strncpy( txt_bufer, argv[1], BUF_SIZ);
	strncpy( &sendbuf[text_len], txt_bufer, BUF_SIZ);
	text_len += strlen(txt_bufer);
	/*
	sendbuf[text_len++] = 't';
	sendbuf[text_len++] = 'e';
	sendbuf[text_len++] = 's';
	sendbuf[text_len++] = 't';
	udph->len = htons(text_len - sizeof(struct ether_header) - sizeof(struct iphdr));
	iph->tot_len = htons(text_len - sizeof(struct ether_header));
	*/

	//Заполнение поля длины в заголовках
	udph->len = htons(text_len - sizeof(struct ether_header) - sizeof(struct iphdr));
	iph->tot_len = htons(text_len - sizeof(struct ether_header));
	
	//Отправка пакета
	struct sockaddr_ll socket_address;
	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	socket_address.sll_halen = ETH_ALEN;
	for(int i = 0; i < LENMAC; i++) {
		socket_address.sll_addr[i] = macserver[i];
	}
	if (sendto(sockfd, sendbuf, text_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0){
		printf("Send failed\n");
	}

	//Приём пакета
	char buf[BUF_SIZ];
	int sockfroms;
	if ((sockfroms = socket(PF_PACKET, SOCK_RAW, htons(ETHERTYPE_IP))) == -1) {
		perror("listener: socket"); 
		return -1;
	}
	struct ifreq ifopts;
	strncpy(ifopts.ifr_name, DEFAULT_IF, IFNAMSIZ-1);
	ioctl(sockfroms, SIOCGIFFLAGS, &ifopts);
	ifopts.ifr_flags |= IFF_PROMISC;
	ioctl(sockfroms, SIOCSIFFLAGS, &ifopts);
	int sockopt;
	if (setsockopt(sockfroms, SOL_SOCKET, SO_REUSEADDR, &sockopt, sizeof(sockopt)) == -1) {
		perror("setsockopt");
		close(sockfroms);
		exit(EXIT_FAILURE);
	}
	if (setsockopt(sockfroms, SOL_SOCKET, SO_BINDTODEVICE, DEFAULT_IF, IFNAMSIZ-1) == -1)  {
		perror("SO_BINDTODEVICE");
		close(sockfroms);
		exit(EXIT_FAILURE);
	}
	ssize_t numbytes;
	char sender[BUF_SIZ];

	while(1) {
		printf("listener: Waiting to recvfrom...\n");
		numbytes = recvfrom(sockfroms, buf, BUF_SIZ, 0, NULL, NULL);
		printf("listener: got packet from server%zd bytes\n", numbytes);

		int check = checkingMAC(buf, sockfroms);

		if(check == 1) {
			struct sockaddr_storage their_addr;
			struct iphdr *iph = (struct iphdr *)(buf + sizeof(struct ether_header));   
			((struct sockaddr_in *)&their_addr)->sin_addr.s_addr = iph->saddr;
			inet_ntop(AF_INET, &(((struct sockaddr_in*)&their_addr)->sin_addr), sender, sizeof(sender));

			if(strcmp(sender, destIP) == 0) {
				infopacket(buf,numbytes);
				close(sockfroms);
				exit(0);
			}
		}
		else continue;
	}
	return 0;
}