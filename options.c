#include "options.h"

int checkingMAC(char *buf, int sock) {

	struct ifreq if_mac;

	struct ether_header *eh = (struct ether_header *)buf;																

	memset(&if_mac, 0, sizeof(struct ifreq));
  	strncpy(if_mac.ifr_name, DEFAULT_IF, IFNAMSIZ-1);

  	if (ioctl(sock, SIOCGIFHWADDR, &if_mac) < 0) {		
    	perror("SIOCGIFHWADDR");							
  	}

	int count = 0;

    for(int i = 0; i < LENMAC; i++) {
        if (eh->ether_dhost[i] == ((uint8_t *)&if_mac.ifr_hwaddr.sa_data)[i]) {
           	count++;
        }
        else {
           	break;
        }
    }

   	if(count == LENMAC) {
   		printf("Correct destination MAC address\n");   
       	return 1;
    } 
    else {
    	printf("Wrong destination MAC: ");
        return 0;
    }

}

void infopacket(char *buf, ssize_t numbytes) {

	char sender[BUF_SIZ];
	struct ether_header *eh = (struct ether_header *) buf;
    struct iphdr *iph = (struct iphdr *) (buf + sizeof(struct ether_header));
    struct udphdr *udph = (struct udphdr *) (buf + sizeof(struct iphdr) + sizeof(struct ether_header));

	printf("Информация о пакете. \n");
	//Ethernet
	printf("Mac-заголовок отправителя: \n");
	printf("Mac-adress source: ");
	for(int i = 0; i < LENMAC; i++) {
		printf("%02x:", eh->ether_shost[i]);
	}
	printf("\n");
	printf("Mac-adress dest: ");
	for(int i = 0; i < LENMAC; i++) {
	    printf("%02x:", eh->ether_dhost[i]);
	}
	printf("\n");
	printf("Ether type: ");
	printf("%02x\n", eh->ether_type);                            
	printf("\n");
	//IP
	printf("Ip-заголовок отправителя: \n");
	printf("ihl: %d\n", iph->ihl);
	printf("version: %d\n", iph->version);
	printf("tos: %d\n", iph->tos);
	printf("id: %d\n", iph->id);
	printf("ttl: %d\n", iph->ttl);
	printf("protocol: %d\n", iph->protocol);
	inet_ntop(AF_INET, &(iph->saddr), sender, sizeof(sender));
	printf("IP-adress Source: %s\n", sender);
	inet_ntop(AF_INET, &(iph->daddr), sender, sizeof(sender));
	printf("Ip-adress dest: %s\n", sender);
	printf("Total length ip header: %d\n", ntohs(iph->tot_len));
	//UDP
	if(iph->protocol == IP_PROTO_FOR_UDP) {
	    printf("UDP-заголовок отправителя: \n");
	    printf("Source port: %d\n", ntohs(udph->source));
	    printf("Dest port: %d\n", ntohs(udph->dest));
	    printf("Check sum: %d\n", udph->check);
	    printf("Length udp: %d\n", ntohs(udph->len));
	    printf("Общий размер пакета: %d\n", numbytes);
	    printf("Поле - данные: %d\n", numbytes-(sizeof(struct ether_header)+sizeof(struct iphdr)+sizeof(struct udphdr)));
	    for (int i = sizeof(struct ether_header)+sizeof(struct iphdr)+sizeof(struct udphdr); i < numbytes; i++) {
	        printf("%c", buf[i]);
	    }
	}
	printf("\n");
}