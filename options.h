#include <stdio.h>
#include <stdlib.h>           // malloc()
#include <unistd.h>           // close()
#include <string.h>           // strcpy, memset(), and memcpy() //
#include <netdb.h>            // struct addrinfo
#include <sys/types.h>        // socket(), uint8_t, uint16_t, uint32_t
#include <sys/socket.h>       // socket()
#include <sys/ioctl.h>        // macro ioctl is defined
#include <netinet/in.h>       // IPPROTO_RAW, IPPROTO_UDP, INET_ADDRSTRLEN
#include <netinet/ip.h>       // struct ip and IP_MAXPACKET (which is 65535)
#include <netinet/udp.h>      // struct udphdr
#include <netinet/ether.h>    // struct ether_header
#include <arpa/inet.h>        // inet_pton() and inet_ntop()
#include <bits/ioctls.h>      // "request" of ioctl
#include <net/if.h>           // struct ifreq
#include <linux/if_ether.h>   // ETH_P_IP = 0x0800, ETH_P_IPV6 = 0x86DD
#include <linux/if_packet.h>  // struct sockaddr_ll
#include <errno.h>

#define LENMAC          6
#define UDP_PROT        17
#define DEFAULT_IF      "wlp2s0"
#define BUF_SIZ         1024
#define SERVER_IP       "192.168.0.3"
#define SERVER_MAC      {c0, 18, 85, 29, 1c, d8}
#define CLIENT_IP       "192.168.0.2"
#define COUNT_CLIENTS   1

#define IHL              5
#define VERSION_IP       4
#define TOS              0
#define ID_IP            1337
#define TTL              64
#define IP_PROTO_FOR_UDP 17 //TCP 6

#define UDP_CLIENT_PORT  2200
#define UDP_SERVER_PORT  2020





int checkingMAC(char *buf, int sock);
void infopacket(char *buf, ssize_t numbytes);